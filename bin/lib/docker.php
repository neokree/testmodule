<?php

/**
 * @param string $imageName
 * @return bool
 */
function dockerImageDoesNotExist($imageName)
{
    return trim(shell_exec("docker image inspect $imageName:latest 2> /dev/null")) === '[]';
}

/**
 * @param string $imageName
 * @param string $dockerfileFolderPath
 */
function buildDockerImage($imageName, $dockerfileFolderPath)
{
    shell_exec("docker build -t $imageName $dockerfileFolderPath");
}

/**
 * @param string $command
 * @param string $imageName
 * @return int
 */
function executeCommandInDockerContainer($command, $imageName)
{
    $returnVal = 0;
    system('docker run --rm -it -v $PWD:/app -w /app '.$imageName.' '.$command, $returnVal);
    return $returnVal;
}