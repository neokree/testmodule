<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    /**
     * Define custom actions here
     */

    public function installModule()
    {
//        date_default_timezone_set('UTC');
        $I = $this;
        $I->amOnPage('/modules/testmodule/helpers/install.php');
    }

    public function uninstallModule()
    {
        $I = $this;
        $I->amOnPage('/modules/testmodule/helpers/uninstall.php');
    }

    public function loginAsAdmin()
    {
        $I = $this;
        $I->amOnPage('/adminPS');
        $I->fillField('email', 'demo@prestashop.com');
        $I->fillField('passwd', 'prestashop_demo');
        $I->click('submitLogin');
        $I->see('Dashboard');
    }

    public function amOnConfigurationPage()
    {
        $I = $this;
        $link = $I->getConfigurationPageLink();

        if ($this->isFullUrl($link)) {
            $I->amOnUrl($link);
        }
        else {
            $I->amOnPage('/adminPS/' . $link);
        }
    }

    /**
     * @return string
     */
    private function getConfigurationPageLink()
    {
        $I = $this;
        $I->amOnPage('/modules/testmodule/helpers/configurationPageLink.php');
        $link = $I->grabTextFrom('p');

        return $link;
    }

    /**
     * @param string $link
     * @return bool
     */
    public function isFullUrl($link)
    {
        return strpos($link, 'http') !== false;
    }
}
