<?php


class ExampleCest
{

    public function _before(FunctionalTester $I)
    {
        $I->installModule();
    }

    public function _after(FunctionalTester $I)
    {
        $I->uninstallModule();
    }

    // tests
    public function tryToTest(FunctionalTester $I)
    {
        $I->wantToTest('something 1');
    }

    public function testSomethingElse(FunctionalTester $I)
    {
        $I->wantToTest('something 2');
    }
}
