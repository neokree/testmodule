<?php

include_once __DIR__ . '/../../../config/config.inc.php';
include_once __DIR__ . '/../testmodule.php';

$module = new Testmodule();
if (Module::isInstalled($module->name))
    $module->uninstall();