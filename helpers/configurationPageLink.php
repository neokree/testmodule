<?php
# Trick PrestaShop to think we are in the BackOffice to load the Employee object (which is required for admin link generation)
$adminPath = realpath(getcwd() . '/../../../adminPS');
define('_PS_ADMIN_DIR_', $adminPath);

// Include necessary files
include_once __DIR__ . '/../../../config/config.inc.php';
include_once __DIR__ . '/../testmodule.php';

// Create link using Link object
$module = new Testmodule();
$context = Context::getContext();
$link = $context->link->getAdminLink('AdminModules', true).'&module_name='.$module->name.'&tab_module='.$module->tab.'&configure='.$module->name;

// Show link (using p tag, so it can be grabbed from Codeception)
echo "<p>$link</p>";